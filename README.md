#GRUPO:	
	Nome                       RA
	Allan Sapucaia Barboza     134796
	Diego Luis de Souza        135491
	Luis Fernando Antonioli	   136710

## Funcionamento
Nesta seção descrevemos o uso da calculadora.

Como foi implementada como driver de caracteres, o uso da calculadora consiste na leitura e escrita do arquivo do sistema *\dev\mycalc*, além de um acumulador que atua como "offset". O usuário deve direcionar uma expressão para o arquivo e depois ler seu conteúdo como no exemplo, respectivamente.

```javascript
echo "1 2 +" > /dev/mycalc
cat /dev/mycalc
```
Toda expressão retornada pela calculadora é somada ao valor de acumulador, ou offset, antes de ser impressa no arquivo. O offset possui três operações: atribuição, leitura e _reset_. Estas operações podem ser acessadas através do arquivo test_ioctl.

Como pode ser visto no exemplo acima, a calculadora interpreta expressões no formato da Notação Polonesa Reversa ( RPN, do inglês _Polish Reverse Notation_) com operandos inteiros. Esse formado é popular entre os fabricantes de calculadoras por eliminar a necessidade do uso de parenteses.

De maneira simplificada, a calculadora empilha os operandos até encontrar um operador, que é então aplicado aos últimos operandos empilhados ( tantos quanto necessário ) e então empilha o resultado. Como exemplo, a expressão

```javascript
2 * (3 + (2 * (1 + 1)))
```

pode ser escrita como

```javascript
1 1 + 2 * 3 + 2 *
```

Para mais detalhes sobre a conversão entre a notação infixa ( usual ) e a RPN, recomendados o estudo do algoritmo _Shunting-yard_ de Edsger Dijkstra.

## Implementação
Nesta seção é descrita a organização da implementação, mais detalhes podem ser encontrados nos comentários do código-fonte. A implementação consiste em três partes distintas: pilha, solver e interface.

A pilha é implementada como lista ligada e é utilizada para processar os operandos.

O solver tem como entrada a expressão escrita no driver e retorna o valor numérico da expressão ou avisa que ocorreu um erro. Essencialmente, o solver processa cada caracter da string. Se o caracter for uma operação, a mesma é aplicada aos ultimos números empilhados ( dependendo do seu número de argumentos ). Se, por outro lado, o caracter for um digito, um operando é construido. A implementação do solver é flexível o bastante para permitir a adição de novas operações n-árias modificando apenas a função que aplica operadores.
	
A interface é responsável por criar o driver propriamente dito e o arquivo de sistema, repassando as leituras e escritas para o solver. Além das funcionalidades necessárias para o funcionamento básico do driver, foram implementadas garantias de validade e consistência dos buffers. Em particular, foram garantidas alocações distintas na memória do usuário e do kernel, copiando quando necessário, e o uso da variável _loof_. 

O acumulador foi implementando através do ioctl, se comportando como uma variável. Sua implementação utiliza funcionalidades básicas da interface ioctl e se encontrar no arquivo test_ioctl, no diretório de arquivos de teste.

## Testes
Como o driver é uma peça crítica do sistema operacional, tendo acesso ao modo kernel, foram criados diversos testes. Esses testes tem o objetivo de garantir  que, mesmo em situação anormais, o driver se comportará corretamente, reportando entradas inválidas, sem parar de funcionar. Mais detalhes sobre os testes podem ser encontrados no diretório correspondente.