#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#define MAX_SIZE 4096

/* Test 1 of the calcDLA driver */
/* Input  expression (RPN): 5 17 2 * + 5 * 1 + 43 * 13 - 2 19 * 37 - * 2 2 * 1 + /    */
/* Input  expressoin converted to infix: (((5 + 17 * 2) * 5 + 1) * 43 - 13) * (2 * 19 - 37) / (2 * 2 + 1)    */
/* Expected result: 1683   */
int main()
{
  int fd;
  char c[200];
  
  fd = open("/dev/mycalc", O_RDWR);
  
  /* beautiful expression*/
  if (write (fd, "5 17 2 * + 5 * 1 + 43 * 13 - 2 19 * 37 - * 2 2 * 1 + /", 54) < 54) {
    perror("calcDLA write");
    return 1;
  }

  read (fd, c, MAX_SIZE);
  printf("Expression: 5 17 2 * + 5 * 1 + 43 * 13 - 2 19 * 37 - * 2 2 * 1 + /\nResult: %s\n", c);

  close (fd);
  return 0;   
}
