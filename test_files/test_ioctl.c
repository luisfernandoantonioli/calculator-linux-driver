#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>

#define CALCDLA_GET_VARIABLES _IOR('q', 1, int *)
#define CALCDLA_CLR_VARIABLES _IO('q', 2)
#define CALCDLA_SET_VARIABLES _IOW('q', 3, int *)

/* Test ioctl operations */
/* Options: -g get accumulator value
            -c clear accumulator (acc = 0)
	    -s set accumulator value
	    default get accumulator value
*/

void get_vars(int fd)
{
  int q;
  if (ioctl(fd, CALCDLA_GET_VARIABLES, &q) == -1)
    {
      perror("calcDLA ioctl get");
    }
  else
    {
      printf("Accumulator : %d\n", q);
    }
}

void clr_vars(int fd)
{
  if (ioctl(fd, CALCDLA_CLR_VARIABLES) == -1)
    {
      perror("calcDLA ioctl clr");
    }
}

void set_vars(int fd)
{
  int q;
  printf("Enter Accumulator: ");
  scanf("%d", &q);
  getchar();

  if (ioctl(fd, CALCDLA_SET_VARIABLES, &q) == -1)
    {
      perror("calcDLA ioctl set");
    }
}
 
int main(int argc, char *argv[])
{
  char *file_name = "/dev/mycalc";
  int fd;
    enum
    {
      e_get,
      e_clr,
      e_set
    } option;
 
    if (argc == 1)
      {
        option = e_get;
      }
    else if (argc == 2)
      {
        if (strcmp(argv[1], "-g") == 0)
	  {
            option = e_get;
	  }
        else if (strcmp(argv[1], "-c") == 0)
	  {
            option = e_clr;
	  }
        else if (strcmp(argv[1], "-s") == 0)
	  {
            option = e_set;
	  }
        else
	  {
            fprintf(stderr, "Usage: %s [-g | -c | -s]\n", argv[0]);
            return 1;
	  }
      }
    else
      {
        fprintf(stderr, "Usage: %s [-g | -c | -s]\n", argv[0]);
        return 1;
      }
    
    fd = open(file_name, O_RDWR);
    if (fd == -1)
      {
        perror("calcDLA open");
        return 2;
      }
 
    switch (option)
      {
      case e_get:
	get_vars(fd);
	break;
      case e_clr:
	clr_vars(fd);
	break;
      case e_set:
	set_vars(fd);
	break;
      default:
	break;
      }
    
    close (fd);
 
    return 0;
}
