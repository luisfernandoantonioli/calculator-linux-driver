#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

/* Test 1 of the calcDLA driver */
/* Input  expression (RPN): \n    */
/* Expected result: INVALID   */
int main()
{
  int fd;
  char c[200];
  
  fd = open("/dev/mycalc", O_RDWR);
  
  /* beautiful expression*/
  if (write (fd, "\n", 54) < 54) {
    perror("calcDLA write");
    return 1;
  }

  if (read (fd, c, 7) < 7) {
    perror("calcDLA read");
    return 1;
  }

  printf("\n /\nResult: %s\n", c);

  close (fd);
  return 0;   
}