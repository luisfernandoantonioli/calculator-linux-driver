#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#define MAX_SIZE 4096

/* Test 1 of the calcDLA driver */
/* Input  expression (RPN): 1 1 +    */
/* Input  expressoin converted to infix: 1 + 1    */
/* Expected result: 2   */
int main()
{
  int fd;
  char c[200];
  
  fd = open("/dev/mycalc", O_RDWR);
  
  /* beautiful expression*/
  if (write (fd, "1 1 +", 5) < 5) {
    perror("calcDLA write");
    return 1;
  }

  read (fd, c, MAX_SIZE);
  printf("Expression: 1 1 +\nResult: %s\n", c);

  close (fd);
  return 0; 
}
