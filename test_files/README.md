#README
## Testes
Nesta pasta encontra-se o código fonte bem como o executável dos programas que foram criados para testar o driver em diversas situações. 
Para utilizá-los basta executar em qualquer pasta do sistema. Após sua execução eles retornam a expressão utilizada junto com a resposta devolvida pelo driver.

### Teste 0
Uma simples operação de soma é testada.

### Teste 1
Uma expressão mais complexa envolvendo todos os operadores suportados pela calculadora é testada.

### Teste 2
Uma expressão que não contem nenhum operador é testada.

### Teste 3
Uma string vazia é passada como expressão.

### Teste 4
Uma expressão composta apenas por um operador e nenhum operando é testada.

### Teste 5
   Uma string que possui caracteres inválidos é passada como expressão

### Teste 6
   Uma string contendo apenas o caractere "\n" é passada como expressão 

### Teste 7
   Uma string com número de caracteres maior que o suportando pelo buffer do driver é testada. Para mais detalhes veja os comentários no código.

### Teste ioctl
   Operações para manipulação do acumulador através do ioctl.

```
Leitura        ./test_ioctl -g
Atribuição     ./test_ioctl -s
Clear          ./test_ioctl -c
```


