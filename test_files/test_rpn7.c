#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX_IN 10000
#define MAX_SIZE 4096

/* Test 1 of the calcDLA driver */
/* Input  expression (RPN): 1 1 + 1 + 1 + ... (1025 times) */
/* Expected result: 1024 (overflow) */
int main()
{
  int fd, i;
  FILE *in;
  char expr[MAX_IN];
  char result[200];
  char c;

  in = fopen("expr_overflow.txt", "r");

  if(in == NULL)
    {
       fprintf(stderr, "Can't open input file expr_overflow.txt!\n");
       exit(1);
    }

  i = 0;
  while ((c = fgetc(in)) != EOF)
    expr[i++] = c;
  expr[i] = '\0';
  
  fd = open("/dev/mycalc", O_RDWR);
  
  /* number of characters 4*1025 - 3*/
  /* overflow - only 4096 characters will be written, so
     the result is 1024 and not 1025 */
  if (write (fd, expr, 4100) < MAX_SIZE) {
    perror("calcDLA write");
    return 1;
  }

  read (fd, result, MAX_SIZE);
  printf("Expression: 1 1 + 1 + 1 + ... (1025 times)\nResult: %s\n", result);

  close (fd);

  fclose(in);
  return 0;   
}
