#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#define MAX_SIZE 4096

/* Test 1 of the calcDLA driver */
/* Input  expression (RPN): 8   */
/* Input  expressoin converted to infix: 8    */
/* Expected result: 8   */
int main()
{
  int fd;
  char c[200];
  
  fd = open("/dev/mycalc", O_RDWR);
  
  /* beautiful expression*/
  if (write (fd, "8", 1) < 1) {
    perror("calcDLA write");
    return 1;
  }

  read (fd, c, MAX_SIZE);
  printf("Expression: 8\nResult: %s\n", c);

  close (fd);
  return 0;   
}
