/* Implement stack data structure for integers */

#ifndef STACK_H
#define STACK_H

#include <linux/slab.h>

/* stack element struct */
struct s_stack_elem
{
  struct s_stack_elem * next;
  int val;
};

typedef struct s_stack_elem stack_elem;

struct s_stack
{
  stack_elem * top;
  int size;
};

typedef struct s_stack stack;

/* create stack*/
void stack_create(stack * s);

/*delete stack and free allocated memory*/
void stack_delete(stack * s);

/* push value val into s */
void stack_push(stack * s,int val);

/* pop the top of s and return it
   May fail if s is empty */
int stack_pop(stack * s);

/* check if s is empty, returning 1 if it's empty and 0 otherwise */
int stack_get_size(stack * s);

#endif
