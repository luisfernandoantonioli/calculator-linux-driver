#include "rpn.h"

int solve(char * expr, int * result)
{
  stack operands; // stack that store the operands
  int len_expr = strlen(expr); // length of the expression
  int operand; // current parsed operand
  int cur = 0; // current positon in the expr string

  //initialize stack
  stack_create(&operands);

  //loop while there is characters to be parsed
  while(cur < len_expr)
    {
      //try to get an operand
      if(get_operand(expr,&cur,&operand) == 0)
	{
	  stack_push(&operands,operand);
	}
      //check for expr overflow
      else if(cur < len_expr)
	{
	  //apply current operator
	  if(apply_operator(&operands,expr[cur]) == 0)
	    {
	      cur++;
	    }
	  else
	    {
	      //invalid expression
	      stack_delete(&operands);
	      return -1;
	    }
	}
    }
  //verify if there is only one remaining operand
  if( stack_get_size(&operands) == 1)
    {
      *result = stack_pop(&operands);
    }
  else
    {
      //invalid expression
      stack_delete(&operands);
      result = NULL;
      return -1;
    }
  return 0;
}

int apply_operator(stack * s, char operator)
{
  int op[2]; //operands' array
  int result; // operation result
  //ignore ' '
  if((operator != ' ') && (operator != '\n')  )
    {
      switch(operator)
	{
	case '+':// addition
	  if(stack_get_size(s) < 2)
	    return -1;
	  op[1] = stack_pop(s);
	  op[0] = stack_pop(s);
	  result = op[0]+op[1];
	  break;
	case '-': // subtraction
	  if(stack_get_size(s) < 2)
	    return -1;
	  op[1] = stack_pop(s);
	  op[0] = stack_pop(s);
	  result = op[0]-op[1];
	  break;
	case '*': //multiplication
	  if(stack_get_size(s) < 2)
	    return -1;
	  op[1] = stack_pop(s);
	  op[0] = stack_pop(s);
	  result = op[0]*op[1];
	  break;
	case '/': //division
	  if(stack_get_size(s) < 2)
	    return -1;
	  op[1] = stack_pop(s);
	  op[0] = stack_pop(s);
	  result = op[0]/op[1];
	  break;
	default:
	  return -1;
	}
      stack_push(s,result);
    }
  return 0;
}

int get_operand(char * expr, int * cur, int * operand)
{
  int number=0; // number being parsed
  int size=0; // number of read characters
  int digit; // current digit
  char c; // current character in expr

  while(1)
    {
      c = expr[*cur];
      if(isdigit(c))
	{
	  digit = c-'0';
	  number = number*10+digit;
	}
      else
	{
	  //end of number
	  break;
	}
      *cur = *cur+1;
      size++;
    }
 
  if(size == 0)
    {
      //no digit was read
      return -1;
    }
  *operand = number;
  return 0;    
}

