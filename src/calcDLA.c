#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/slab.h>

#include "calcDLA.h"
#include "rpn.h"

#define MAX_SIZE 4096

static dev_t deviceNumber;        // first device number
static struct cdev c_dev;         // character device structure
static struct class *deviceClass; // device class

static int acc = 0; // accumulator. Default 0
static char calc_result[MAX_SIZE]; //store operation result
static char calc_expression[MAX_SIZE]; //store operation expression

static int my_open(struct inode *i, struct file *f)
{
  printk(KERN_INFO "Calculator Driver: open()\n");
  return 0;
}

static int my_close(struct inode *i, struct file *f)
{
  printk(KERN_INFO "Calculator Driver: close()\n");
  return 0;
}

static ssize_t my_read(struct file *f, char __user *buf, size_t len, loff_t *off)
{
  printk(KERN_INFO "Calculator Driver: read()\n");
  if(len > strlen(calc_result))
    {
      len = strlen(calc_result);
    }

  if((*off) > strlen(calc_result))
    {
      return 0; /* End of file */
    }
  else if (copy_to_user(buf, calc_result + (*off), len))
    {
      return -EFAULT;
    }
  else 
    { 
      (*off) += len;
      return len;
    }
}

static ssize_t my_write(struct file *f, const char __user *buf, size_t len, loff_t *off)
{
  printk(KERN_INFO "Calculator Driver: write()\n");
  memset(calc_expression,0,MAX_SIZE);
  memset(calc_result,0,MAX_SIZE);
  if(len > MAX_SIZE)
    {
      len = MAX_SIZE;
    }

  if (copy_from_user(calc_expression, buf, len) != 0)
    return -EFAULT;
  else
    {
      //return result of calc_expression in calc_result
      int ans;
      if(solve(calc_expression, &ans) != 0)
	{
	  sprintf(calc_result, "INVALID");
	}
      else
	{
	  ans +=  acc;
	  sprintf(calc_result, "%d", ans);
	}
      return len;
    }
}

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
static int my_ioctl(struct inode *i, struct file *f, unsigned int cmd, unsigned long arg)
#else
  static long my_ioctl(struct file *f, unsigned int cmd, unsigned long arg)
#endif
{
  int v_acc;

  switch (cmd)
    {
    case CALCDLA_GET_VARIABLES:
      v_acc = acc;
      if (copy_to_user((int *)arg, &v_acc, sizeof(int)))
	{
	  return -EACCES;
	}
      break;
    case CALCDLA_CLR_VARIABLES:
      acc = 0;
      break;
    case CALCDLA_SET_VARIABLES:
      if (copy_from_user(&v_acc, (int *)arg, sizeof(int)))
	{
	  return -EACCES;
	}
      acc = v_acc;
      break;
    default:
      return -EINVAL;
    }
  return 0;
}

static struct file_operations calc_fops =
  {
    .owner = THIS_MODULE,
    .open = my_open,
    .release = my_close,
    .read = my_read,
    .write = my_write,
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35))
    .ioctl = my_ioctl,
#else
    .unlocked_ioctl = my_ioctl
#endif
  };
 
static int __init calcDLA_init(void) /* Constructor */
{
  printk(KERN_INFO "calcDLA registered");
  if (alloc_chrdev_region(&deviceNumber, 0, 1, "calc_rpn") < 0)
    {
      return -1;
    }
  if ((deviceClass = class_create(THIS_MODULE, "chardrv")) == NULL)
    {
      unregister_chrdev_region(deviceNumber, 1);
      return -1;
    }
  if (device_create(deviceClass, NULL, deviceNumber, NULL, "mycalc") == NULL)
    {
      class_destroy(deviceClass);
      unregister_chrdev_region(deviceNumber, 1);
      return -1;
    }
  cdev_init(&c_dev, &calc_fops);
  if (cdev_add(&c_dev, deviceNumber, 1) == -1)
    {
      device_destroy(deviceClass, deviceNumber);
      class_destroy(deviceClass);
      unregister_chrdev_region(deviceNumber, 1);
      return -1;
    }
  return 0;
}
 
static void __exit calcDLA_exit(void) /* Destructor */
{
  cdev_del(&c_dev);
  device_destroy(deviceClass, deviceNumber);
  class_destroy(deviceClass);
  unregister_chrdev_region(deviceNumber, 1);
  printk(KERN_INFO "RPN Calculator Driver exit");
}
 
module_init(calcDLA_init);
module_exit(calcDLA_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Luis, Diego, Allan <no_email_at_nowhereland_dot_com>");
MODULE_DESCRIPTION("RPN_Calculator_driver");
