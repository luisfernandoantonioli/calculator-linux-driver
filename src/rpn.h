/* parse Reverse Polish Notation expressions */

#ifndef RPN_H
#define RPN_H

#include <linux/string.h>
#include <linux/ctype.h>
#include "stack.h"

/* parse and solve expr, placing the result in result. Return non-zero in case of failure */
int solve(char * expr, int * result);

/* Apply an operator to the elements of stack s. Returns non-zero in case of failure */ 
int apply_operator(stack * s,char operator);

/* Gets the next operand of a giving position of expr, placing it in operand and updating cur. If there is an operand, returns 0. */
/* WARNING: only parse INTEGERS */
int get_operand(char * expr,int * cur,int * operand);

#endif
