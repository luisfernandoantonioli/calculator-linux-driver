#ifndef calcDLA_H
#define calcDLA_H
#include <linux/ioctl.h>

#define CALCDLA_GET_VARIABLES _IOR('q', 1, int *)
#define CALCDLA_CLR_VARIABLES _IO('q', 2)
#define CALCDLA_SET_VARIABLES _IOW('q', 3, int *)

#endif
