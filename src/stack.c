#include "stack.h"

/* stack */
void stack_create(stack * s)
{
  s->size = 0;
  s->top = NULL;
}

void stack_delete(stack * s)
{
  stack_elem *next;
  while(s->top != NULL)
    {
      next = s->top->next;
      kfree(s->top);
      s->top = next;
    }
  s->size = 0;
}

void stack_push(stack * s,int val)
{
  stack_elem * new;
  if(s->top == NULL)
    {
      s->top = kmalloc(sizeof(stack), GFP_KERNEL);
      s->top->next = NULL;
    }
  else
    {
      new = kmalloc(sizeof(stack), GFP_KERNEL);
      new->next = s->top;
      s->top = new;
    }						
  s->top->val = val;
  s->size++;
}
int stack_pop(stack * s)
{
  stack_elem * next = s->top->next;
  int val = s->top->val;

  kfree(s->top);
  s->top = next;
  s->size--;
  return val;
}

int stack_get_size(stack * s)
{
  return s->size;
}
